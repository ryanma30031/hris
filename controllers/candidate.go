package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/ryanma3003/hris/db"
	"github.com/ryanma3003/hris/models"
)

func CandidateIndex(c *gin.Context) {
	var candidates []models.Candidate
	err := db.DB.Find(&candidates).Error

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
	}

	c.JSON(http.StatusOK, gin.H{
		"data": candidates,
	})
}
